#######################
## STEP 1: Build JAR ##
#######################
# TODO: use 17-jdk when fixed
FROM eclipse-temurin:11-jdk AS builder

WORKDIR /app

# Install dependencies first
COPY .mvn .mvn
COPY pom.xml mvnw ./

RUN chmod +x mvnw
RUN ./mvnw dependency:go-offline

# Build application
COPY src src
RUN ./mvnw -Dmaven.test.skip=true package

###############################
## STEP 2: Build small image ##
###############################
# TODO: use 17-jre when available
FROM eclipse-temurin:11-jre

# Copy the JAR from the builder
COPY --from=builder /app/target/*.jar /app.jar

# Create a non-root user
RUN groupadd -r java && useradd -g java javauser
USER javauser

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app.jar"]
