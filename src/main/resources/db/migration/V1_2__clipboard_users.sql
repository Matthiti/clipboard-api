CREATE TABLE `clipboard_users` (
    `clipboard_id` BIGINT UNSIGNED NOT NULL,
    `user_id` BIGINT UNSIGNED NOT NULL,
    `encryption_key` VARCHAR(344) NOT NULL,
    `is_owner` BOOLEAN NOT NULL,

    PRIMARY KEY (`clipboard_id`, `user_id`),
    FOREIGN KEY (`clipboard_id`) REFERENCES `clipboards`(`id`),
    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`)
);

-- Copy old format to new format
INSERT INTO `clipboard_users` (`clipboard_id`, `user_id`, `encryption_key`, `is_owner`)
SELECT c.`id`, c.`user_id`, c.`encryption_key`, TRUE
FROM clipboards c;

ALTER TABLE `clipboards`
DROP FOREIGN KEY `clipboards_ibfk_1`,
DROP COLUMN `user_id`,
DROP COLUMN `encryption_key`