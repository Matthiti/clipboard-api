CREATE TABLE `users` (
    `id` BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `first_name` VARCHAR(64) NOT NULL,
    `last_name` VARCHAR(64) NOT NULL,
    `email` VARCHAR(64) UNIQUE NOT NULL,
    `password` VARCHAR(255) NOT NULL,
    `registered_at` BIGINT UNSIGNED NOT NULL,
    `key_encryption_salt` VARCHAR(24) NOT NULL,
    `master_private_key_iv` VARCHAR(24) NOT NULL,
    `master_private_key` VARCHAR(2176) NOT NULL,
    `master_public_key` VARCHAR(392) NOT NULL,

    INDEX(`email`)
);

CREATE TABLE `clipboards` (
    `id` BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `user_id` BIGINT UNSIGNED NOT NULL,
    `name` VARCHAR(64) NOT NULL,
    `encryption_key` VARCHAR(344) NOT NULL,

    FOREIGN KEY (`user_id`) REFERENCES `users`(`id`)
);

CREATE TABLE `clipboard_items` (
    `id` BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `clipboard_id` BIGINT UNSIGNED NOT NULL,
    `encrypted_content` TEXT NOT NULL,
    `encryption_iv` VARCHAR(24) NOT NULL,
    `pasted_at` BIGINT UNSIGNED NOT NULL,

    FOREIGN KEY (`clipboard_id`) REFERENCES `clipboards`(`id`)
);