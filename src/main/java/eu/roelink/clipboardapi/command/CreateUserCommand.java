package eu.roelink.clipboardapi.command;

import eu.roelink.clipboardapi.dto.request.RegisterRequestDto;
import eu.roelink.clipboardapi.exception.EmailAlreadyRegisteredException;
import eu.roelink.clipboardapi.service.AuthService;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.util.Scanner;

@ShellComponent
public class CreateUserCommand {

    private final AuthService authService;

    private final Scanner scanner;

    public CreateUserCommand(AuthService authService) {
        this.authService = authService;
        this.scanner = new Scanner(System.in);
    }

    @ShellMethod("Create a new user.")
    public String createUser(
        @ShellOption(defaultValue = "") String firstName,
        @ShellOption(defaultValue = "") String lastName,
        @ShellOption(defaultValue = "") String email,
        @ShellOption(defaultValue = "") String password
    ) {
        while (firstName.isBlank()) {
            firstName = getInput("First name: ");
        }

        while (lastName.isBlank()) {
            lastName = getInput("Last name: ");
        }

        while (email.isBlank()) {
            email = getInput("Email: ");
        }

        while (password.isBlank()) {
            password = getInput("Password: ");
        }

        RegisterRequestDto requestDto = new RegisterRequestDto()
            .setFirstName(firstName)
            .setLastName(lastName)
            .setEmail(email)
            .setPassword(password);

        try {
            authService.register(requestDto);
        } catch (EmailAlreadyRegisteredException e) {
            return "ERROR - Email address is already registered";
        }

        return "User created successfully";
    }

    private String getInput(String message) {
        System.out.println(message);
        return scanner.next();
    }
}
