package eu.roelink.clipboardapi.repository;

import eu.roelink.clipboardapi.model.ClipboardItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ClipboardItemRepository extends CrudRepository<ClipboardItem, Long> {

    List<ClipboardItem> findByClipboardIdOrderByPastedAtDesc(Long userId);

    Optional<ClipboardItem> findByClipboardIdAndId(Long clipboardId, Long id);
}
