package eu.roelink.clipboardapi.repository;

import eu.roelink.clipboardapi.model.Clipboard;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ClipboardRepository extends CrudRepository<Clipboard, Long> {

    List<Clipboard> findAllByClipboardUsersUserId(Long userId);

    Optional<Clipboard> findByIdAndClipboardUsersUserId(Long id, Long userId);

    boolean existsByIdAndClipboardUsersUserId(Long id, Long userId);
}
