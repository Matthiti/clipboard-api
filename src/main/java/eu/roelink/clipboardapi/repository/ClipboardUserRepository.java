package eu.roelink.clipboardapi.repository;

import eu.roelink.clipboardapi.model.ClipboardUser;
import org.springframework.data.repository.CrudRepository;

public interface ClipboardUserRepository extends CrudRepository<ClipboardUser, ClipboardUser.ClipboardUserId> {
}
