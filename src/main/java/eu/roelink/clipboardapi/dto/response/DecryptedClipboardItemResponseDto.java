package eu.roelink.clipboardapi.dto.response;

import eu.roelink.clipboardapi.model.DecryptedClipboardItem;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DecryptedClipboardItemResponseDto {

    private Long id;

    private String content;

    private Long pastedAt;

    public static DecryptedClipboardItemResponseDto from(DecryptedClipboardItem item) {
        return new DecryptedClipboardItemResponseDto()
            .setId(item.getId())
            .setContent(item.getContent())
            .setPastedAt(item.getPastedAt());
    }
}
