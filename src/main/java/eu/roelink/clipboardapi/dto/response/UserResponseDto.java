package eu.roelink.clipboardapi.dto.response;

import eu.roelink.clipboardapi.model.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class UserResponseDto {

    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private Long registeredAt;

    private RoleResponseDto role;

    public static UserResponseDto from(User user) {
        return new UserResponseDto()
            .setId(user.getId())
            .setFirstName(user.getFirstName())
            .setLastName(user.getLastName())
            .setEmail(user.getEmail())
            .setRegisteredAt(user.getRegisteredAt())
            .setRole(RoleResponseDto.from(user.getRole()));
    }
}
