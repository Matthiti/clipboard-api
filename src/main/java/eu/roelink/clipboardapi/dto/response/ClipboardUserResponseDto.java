package eu.roelink.clipboardapi.dto.response;

import eu.roelink.clipboardapi.model.ClipboardUser;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class ClipboardUserResponseDto {

    private Long userId;

    private Boolean isOwner;

    public static ClipboardUserResponseDto from(ClipboardUser clipboardUser) {
        return new ClipboardUserResponseDto()
            .setUserId(clipboardUser.getUserId())
            .setIsOwner(clipboardUser.getIsOwner());
    }
}
