package eu.roelink.clipboardapi.dto.response;

import eu.roelink.clipboardapi.model.Role;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class RoleResponseDto {

    private Long id;

    private String name;

    public static RoleResponseDto from(Role role) {
        return new RoleResponseDto()
            .setId(role.getId())
            .setName(role.getName());
    }
}
