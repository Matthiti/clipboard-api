package eu.roelink.clipboardapi.dto.response;

import eu.roelink.clipboardapi.model.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DisplayUserResponseDto {

    private Long id;

    private String firstName;

    private String lastName;

    public static DisplayUserResponseDto from(User user) {
        return new DisplayUserResponseDto()
            .setId(user.getId())
            .setFirstName(user.getFirstName())
            .setLastName(user.getLastName());
    }
}
