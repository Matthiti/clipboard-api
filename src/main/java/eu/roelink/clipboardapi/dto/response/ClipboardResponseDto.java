package eu.roelink.clipboardapi.dto.response;

import eu.roelink.clipboardapi.model.Clipboard;
import eu.roelink.clipboardapi.model.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Accessors(chain = true)
public class ClipboardResponseDto {

    private Long id;

    private String name;

    private List<ClipboardUserResponseDto> users;

    public static ClipboardResponseDto from(Clipboard clipboard) {
        return new ClipboardResponseDto()
            .setId(clipboard.getId())
            .setName(clipboard.getName())
            .setUsers(clipboard.getClipboardUsers().stream().map(ClipboardUserResponseDto::from).collect(Collectors.toList()));
    }
}
