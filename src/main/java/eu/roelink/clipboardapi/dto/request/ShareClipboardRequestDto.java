package eu.roelink.clipboardapi.dto.request;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class ShareClipboardRequestDto {

    @NotNull
    private List<Long> userIds;
}
