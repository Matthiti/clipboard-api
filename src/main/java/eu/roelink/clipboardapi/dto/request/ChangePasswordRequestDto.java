package eu.roelink.clipboardapi.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChangePasswordRequestDto {

    @NotNull
    private String oldPassword;

    @NotNull
    private String newPassword;
}
