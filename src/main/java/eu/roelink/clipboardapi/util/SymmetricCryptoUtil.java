package eu.roelink.clipboardapi.util;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.KeySpec;
import java.util.Base64;

public class SymmetricCryptoUtil {

    public static final String KEY_CIPHER_ALGORITHM = "AES";
    public static final String KEY_DERIVATION_ALGORITHM = "PBKDF2WithHmacSHA256";
    public static final String CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";
    public static final int SALT_LENGTH = 16;
    public static final int ITERATION_COUNT = 65536;
    public static final int KEY_LENGTH = 256;
    public static final int IV_LENGTH = 16;

    public static SecretKey deriveKey(byte[] salt, String password) {
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH);
        try {
            SecretKeyFactory factory = SecretKeyFactory.getInstance(KEY_DERIVATION_ALGORITHM);
            return new SecretKeySpec(factory.generateSecret(spec).getEncoded(), KEY_CIPHER_ALGORITHM);
        } catch (GeneralSecurityException e) {
            // Should never happen
            return null;
        }
    }

    public static SecretKey generateKey() {
        KeyGenerator keyGen;
        try {
            keyGen = KeyGenerator.getInstance(KEY_CIPHER_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            // Should never happen
            return null;
        }
        keyGen.init(KEY_LENGTH);
        return keyGen.generateKey();
    }

    public static SecretKey keyFromBytes(byte[] encoded) {
        return new SecretKeySpec(encoded, KEY_CIPHER_ALGORITHM);
    }

    public static String encrypt(SecretKey key, IvParameterSpec iv, String plainText) {
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            byte[] cipherText = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(cipherText);
        } catch (GeneralSecurityException e) {
            // Should never happen
            return null;
        }
    }

    public static String decrypt(SecretKey key, IvParameterSpec iv, String cipherText) {
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
            byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(cipherText));
            return new String(plainText, StandardCharsets.UTF_8);
        } catch (GeneralSecurityException e) {
            // Should never happen
            return null;
        }
    }

    public static IvParameterSpec generateIv() {
        byte[] iv = new byte[IV_LENGTH];
        new SecureRandom().nextBytes(iv);
        return new IvParameterSpec(iv);
    }

    public static byte[] generateSalt() {
        byte[] salt = new byte[SALT_LENGTH];
        new SecureRandom().nextBytes(salt);
        return salt;
    }
}
