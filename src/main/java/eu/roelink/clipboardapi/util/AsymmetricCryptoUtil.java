package eu.roelink.clipboardapi.util;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class AsymmetricCryptoUtil {

    public static final String CIPHER_ALGORITHM = "RSA";
    public static final int KEY_LENGTH = 2048;

    public static KeyPair generateKey() {
        KeyPairGenerator keyPairGen;
        try {
            keyPairGen = KeyPairGenerator.getInstance(CIPHER_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            // Should never happen
            return null;
        }
        keyPairGen.initialize(KEY_LENGTH);
        return keyPairGen.generateKeyPair();
    }

    public static PrivateKey privateKeyFromBytes(byte[] encoded) {
        KeySpec keySpec = new PKCS8EncodedKeySpec(encoded);
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(CIPHER_ALGORITHM);
            return keyFactory.generatePrivate(keySpec);
        } catch (GeneralSecurityException e) {
            // Should never happen
            return null;
        }
    }

    public static PublicKey publicKeyFromBytes(byte[] encoded) {
        KeySpec keySpec = new X509EncodedKeySpec(encoded);
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(CIPHER_ALGORITHM);
            return keyFactory.generatePublic(keySpec);
        } catch (GeneralSecurityException e) {
            // Should never happen
            return null;
        }
    }

    public static String encrypt(PublicKey publicKey, String plainText) {
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] cipherText = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));
            return Base64.getEncoder().encodeToString(cipherText);
        } catch (GeneralSecurityException e) {
            // Should never happen
            return null;
        }
    }

    public static String decrypt(PrivateKey privateKey, String cipherText) {
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(CIPHER_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(cipherText));
            return new String(plainText, StandardCharsets.UTF_8);
        } catch (GeneralSecurityException e) {
            // Should never happen
            return null;
        }
    }
}
