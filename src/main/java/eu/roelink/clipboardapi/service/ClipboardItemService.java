package eu.roelink.clipboardapi.service;

import eu.roelink.clipboardapi.dto.request.ClipboardItemRequestDto;
import eu.roelink.clipboardapi.exception.ModelNotFoundException;
import eu.roelink.clipboardapi.model.Clipboard;
import eu.roelink.clipboardapi.model.ClipboardItem;
import eu.roelink.clipboardapi.model.DecryptedClipboardItem;
import eu.roelink.clipboardapi.repository.ClipboardItemRepository;
import eu.roelink.clipboardapi.util.AsymmetricCryptoUtil;
import eu.roelink.clipboardapi.util.SymmetricCryptoUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.servlet.http.HttpSession;
import java.security.PrivateKey;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ClipboardItemService extends BaseService {

    private final ClipboardService clipboardService;

    private final ClipboardItemRepository clipboardItemRepository;

    public List<DecryptedClipboardItem> getAllDecryptedClipboardItems(HttpSession session, Long clipboardId) throws ModelNotFoundException {
        Clipboard clipboard = clipboardService.getClipboard(clipboardId);

        List<ClipboardItem> items = clipboardItemRepository.findByClipboardIdOrderByPastedAtDesc(clipboardId);
        SecretKey clipboardKey = getClipboardKey(session, clipboard);

        return items
            .stream()
            .map(item -> decryptItem(clipboardKey, item))
            .collect(Collectors.toList());
    }

    public ClipboardItem getClipboardItem(Long clipboardId, Long itemId) throws ModelNotFoundException {
        if (!clipboardService.existsClipboard(clipboardId)) {
            throw new ModelNotFoundException();
        }

        return clipboardItemRepository.findByClipboardIdAndId(clipboardId, itemId)
            .orElseThrow(ModelNotFoundException::new);
    }

    public DecryptedClipboardItem getDecryptedClipboardItem(HttpSession session, Long clipboardId, Long itemId) throws ModelNotFoundException {
        if (!clipboardService.existsClipboard(clipboardId)) {
            throw new ModelNotFoundException();
        }

        ClipboardItem item = getClipboardItem(clipboardId, itemId);
        return decryptItem(session, item);
    }

    public DecryptedClipboardItem createClipboardItem(HttpSession session, Long clipboardId, ClipboardItemRequestDto clipboardItemRequestDto) throws ModelNotFoundException {
        Clipboard clipboard = clipboardService.getClipboard(clipboardId);

        SecretKey clipboardKey = getClipboardKey(session, clipboard);
        IvParameterSpec iv = SymmetricCryptoUtil.generateIv();
        String content = SymmetricCryptoUtil.encrypt(clipboardKey, iv, clipboardItemRequestDto.getContent());

        ClipboardItem item = new ClipboardItem()
            .setClipboard(clipboard)
            .setEncryptedContent(content)
            .setEncryptionIv(Base64.getEncoder().encodeToString(iv.getIV()))
            .setPastedAt(System.currentTimeMillis() / 1000);

        item = clipboardItemRepository.save(item);

        return new DecryptedClipboardItem()
            .setId(item.getId())
            .setContent(clipboardItemRequestDto.getContent())
            .setPastedAt(item.getPastedAt());
    }

    public DecryptedClipboardItem updateClipboardItem(HttpSession session, Long clipboardId, Long itemId, ClipboardItemRequestDto clipboardItemRequestDto) throws ModelNotFoundException {
        ClipboardItem item = getClipboardItem(clipboardId, itemId);

        SecretKey clipboardKey = getClipboardKey(session, item.getClipboard());
        IvParameterSpec iv = SymmetricCryptoUtil.generateIv();
        String content = SymmetricCryptoUtil.encrypt(clipboardKey, iv, clipboardItemRequestDto.getContent());

        item.setEncryptedContent(content)
            .setEncryptionIv(Base64.getEncoder().encodeToString(iv.getIV()));

        item = clipboardItemRepository.save(item);

        return new DecryptedClipboardItem()
            .setId(item.getId())
            .setContent(clipboardItemRequestDto.getContent())
            .setPastedAt(item.getPastedAt());
    }

    public void deleteClipboardItem(Long clipboardId, Long itemId) throws ModelNotFoundException {
        ClipboardItem item = getClipboardItem(clipboardId, itemId);
        clipboardItemRepository.delete(item);
    }

    private DecryptedClipboardItem decryptItem(HttpSession session, ClipboardItem item) {
        return decryptItem(getClipboardKey(session, item.getClipboard()), item);
    }

    private DecryptedClipboardItem decryptItem(SecretKey clipboardKey, ClipboardItem item) {
        return new DecryptedClipboardItem()
            .setId(item.getId())
            .setContent(decryptContent(clipboardKey, item))
            .setPastedAt(item.getPastedAt());
    }

    private String decryptContent(SecretKey clipboardKey, ClipboardItem item) {
        IvParameterSpec iv = new IvParameterSpec(Base64.getDecoder().decode(item.getEncryptionIv()));
        return SymmetricCryptoUtil.decrypt(clipboardKey, iv, item.getEncryptedContent());
    }

    private SecretKey getClipboardKey(HttpSession session, Clipboard clipboard) {
        PrivateKey privateKey = getMasterPrivateKey(session);
        String encryptedKey = clipboard.getClipboardUsers()
            .stream()
            .filter(u -> u.getUserId().equals(getLoggedInUser().getId()))
            .findFirst()
            .get()
            .getEncryptionKey();

        String clipboardKeyStr = AsymmetricCryptoUtil.decrypt(privateKey, encryptedKey);
        return SymmetricCryptoUtil.keyFromBytes(Base64.getDecoder().decode(clipboardKeyStr));
    }
}
