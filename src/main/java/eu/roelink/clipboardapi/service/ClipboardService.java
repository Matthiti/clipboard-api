package eu.roelink.clipboardapi.service;

import eu.roelink.clipboardapi.dto.request.ClipboardRequestDto;
import eu.roelink.clipboardapi.dto.request.ShareClipboardRequestDto;
import eu.roelink.clipboardapi.exception.ModelNotFoundException;
import eu.roelink.clipboardapi.exception.NotOwnerOfClipboardException;
import eu.roelink.clipboardapi.model.Clipboard;
import eu.roelink.clipboardapi.model.ClipboardUser;
import eu.roelink.clipboardapi.model.User;
import eu.roelink.clipboardapi.repository.ClipboardRepository;
import eu.roelink.clipboardapi.repository.ClipboardUserRepository;
import eu.roelink.clipboardapi.util.AsymmetricCryptoUtil;
import eu.roelink.clipboardapi.util.SymmetricCryptoUtil;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ClipboardService extends BaseService {

    private final ClipboardRepository clipboardRepository;

    private final ClipboardUserRepository clipboardUserRepository;

    private UserService userService;

    public List<Clipboard> getAllClipboardsOfLoggedInUser() {
        return getAllClipboardsOf(getLoggedInUser());
    }

    public List<Clipboard> getAllClipboardsOf(User user) {
        return clipboardRepository.findAllByClipboardUsersUserId(user.getId());
    }

    public Clipboard getClipboard(Long id) throws ModelNotFoundException {
        return clipboardRepository.findByIdAndClipboardUsersUserId(id, getLoggedInUser().getId())
            .orElseThrow(ModelNotFoundException::new);
    }

    @Transactional
    public Clipboard createClipboard(ClipboardRequestDto clipboardRequestDto) {
        User user = getLoggedInUser();
        PublicKey publicKey = AsymmetricCryptoUtil.publicKeyFromBytes(Base64.getDecoder().decode(user.getMasterPublicKey()));
        SecretKey encryptionKey = SymmetricCryptoUtil.generateKey();
        String encryptedKey = AsymmetricCryptoUtil.encrypt(publicKey, Base64.getEncoder().encodeToString(encryptionKey.getEncoded()));

        Clipboard clipboard = new Clipboard()
            .setName(clipboardRequestDto.getName());

        clipboard = clipboardRepository.save(clipboard);

        ClipboardUser clipboardUser = new ClipboardUser()
            .setClipboardId(clipboard.getId())
            .setUserId(user.getId())
            .setEncryptionKey(encryptedKey)
            .setIsOwner(true);

        clipboardUser = clipboardUserRepository.save(clipboardUser);
        clipboard.setClipboardUsers(List.of(clipboardUser));

        return clipboard;
    }

    public Clipboard updateClipboard(Long id, ClipboardRequestDto clipboardRequestDto) throws ModelNotFoundException, NotOwnerOfClipboardException {
        Clipboard clipboard = getClipboard(id);
        if (!clipboard.isOwner(getLoggedInUser())) {
            throw new NotOwnerOfClipboardException();
        }
        clipboard.setName(clipboardRequestDto.getName());
        return clipboardRepository.save(clipboard);
    }

    @Transactional
    public void shareClipboard(HttpSession session, Long id, ShareClipboardRequestDto shareClipboardRequestDto) throws ModelNotFoundException, NotOwnerOfClipboardException {
        Clipboard clipboard = getClipboard(id);
        if (!clipboard.isOwner(getLoggedInUser())) {
            throw new NotOwnerOfClipboardException();
        }

        List<User> users = shareClipboardRequestDto.getUserIds()
            .stream()
            .filter(u -> !u.equals(getLoggedInUser().getId()))
            .distinct()
            .map(userId -> {
                try {
                    return userService.getUser(userId);
                } catch (ModelNotFoundException e) {
                    return null;
                }
            })
            .collect(Collectors.toList());

        if (users.contains(null)) {
            throw new ModelNotFoundException("User");
        }

        // First, delete the old objects
        clipboardUserRepository.deleteAll(
            clipboard.getClipboardUsers()
                .stream()
                .filter(u -> !u.getUserId().equals(getLoggedInUser().getId()))
                .collect(Collectors.toList())
        );

        PrivateKey privateKey = getMasterPrivateKey(session);
        String encryptedKey = clipboard.getClipboardUsers()
            .stream()
            .filter(u -> u.getUserId().equals(getLoggedInUser().getId()))
            .findFirst()
            .get()
            .getEncryptionKey();

        String clipboardKeyStr = AsymmetricCryptoUtil.decrypt(privateKey, encryptedKey);
        SecretKey encryptionKey = SymmetricCryptoUtil.keyFromBytes(Base64.getDecoder().decode(clipboardKeyStr));

        for (User user : users) {
            PublicKey publicKey = AsymmetricCryptoUtil.publicKeyFromBytes(Base64.getDecoder().decode(user.getMasterPublicKey()));
            String reEncryptedKey = AsymmetricCryptoUtil.encrypt(publicKey, Base64.getEncoder().encodeToString(encryptionKey.getEncoded()));

            ClipboardUser clipboardUser = new ClipboardUser()
                .setClipboardId(clipboard.getId())
                .setUserId(user.getId())
                .setEncryptionKey(reEncryptedKey)
                .setIsOwner(false);

            clipboardUserRepository.save(clipboardUser);
        }
    }

    public void deleteClipboard(Long id) throws ModelNotFoundException, NotOwnerOfClipboardException {
        Clipboard clipboard = getClipboard(id);
        if (!clipboard.isOwner(getLoggedInUser())) {
            throw new NotOwnerOfClipboardException();
        }

        clipboardRepository.delete(clipboard);
    }

    public boolean existsClipboard(Long id) {
        return existsClipboard(id, getLoggedInUser());
    }

    public boolean existsClipboard(Long id, User user) {
        return clipboardRepository.existsByIdAndClipboardUsersUserId(id, user.getId());
    }
}
