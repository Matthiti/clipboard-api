package eu.roelink.clipboardapi.service;

import eu.roelink.clipboardapi.dto.request.UserRequestDto;
import eu.roelink.clipboardapi.exception.EmailAlreadyRegisteredException;
import eu.roelink.clipboardapi.exception.ModelNotFoundException;
import eu.roelink.clipboardapi.model.User;
import eu.roelink.clipboardapi.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserService extends BaseService {

    private final UserRepository userRepository;

    public List<User> getAllUsers() {
        return Streamable.of(userRepository.findAll()).toList();
    }

    public User getUser(Long id) throws ModelNotFoundException {
        return userRepository.findById(id)
            .orElseThrow(ModelNotFoundException::new);
    }

    public User updateUser(Long id, UserRequestDto userRequestDto) throws ModelNotFoundException, EmailAlreadyRegisteredException {
        User user = getUser(id);
        user.setFirstName(userRequestDto.getFirstName())
            .setLastName(userRequestDto.getLastName())
            .setEmail(userRequestDto.getEmail());

        if (userRepository.existsByEmailAndIdNot(user.getEmail(), user.getId())) {
            throw new EmailAlreadyRegisteredException();
        }

        return userRepository.save(user);
    }

    public void deleteUser(Long id) throws ModelNotFoundException {
        User user = getUser(id);
        userRepository.delete(user);
    }

    public boolean userExists(Long id) {
        return userRepository.existsById(id);
    }
}
