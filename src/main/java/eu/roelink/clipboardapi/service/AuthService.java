package eu.roelink.clipboardapi.service;

import eu.roelink.clipboardapi.dto.request.ChangePasswordRequestDto;
import eu.roelink.clipboardapi.dto.request.LoginRequestDto;
import eu.roelink.clipboardapi.dto.request.RegisterRequestDto;
import eu.roelink.clipboardapi.exception.EmailAlreadyRegisteredException;
import eu.roelink.clipboardapi.exception.IncorrectOldPasswordException;
import eu.roelink.clipboardapi.model.Role;
import eu.roelink.clipboardapi.model.User;
import eu.roelink.clipboardapi.repository.RoleRepository;
import eu.roelink.clipboardapi.repository.UserRepository;
import eu.roelink.clipboardapi.util.AsymmetricCryptoUtil;
import eu.roelink.clipboardapi.util.SymmetricCryptoUtil;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.servlet.http.HttpSession;
import java.security.KeyPair;
import java.util.Base64;

@Service
@AllArgsConstructor
public class AuthService extends BaseService {

    private final AuthenticationManager authManager;

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    public void login(HttpSession session, LoginRequestDto loginRequestDto) {
        Authentication auth = authenticate(loginRequestDto.getEmail(), loginRequestDto.getPassword());
        createAuthSession(session, auth, loginRequestDto.getPassword());
    }

    public void logout(HttpSession session) {
        session.invalidate();
    }

    public void register(/* HttpSession session ,*/ RegisterRequestDto registerRequestDto) throws EmailAlreadyRegisteredException {
        if (userRepository.existsByEmail(registerRequestDto.getEmail())) {
            throw new EmailAlreadyRegisteredException();
        }

        // Generate key derived from password
        byte[] salt = SymmetricCryptoUtil.generateSalt();
        SecretKey derivedKey = SymmetricCryptoUtil.deriveKey(salt, registerRequestDto.getPassword());

        // Generate encryption key
        KeyPair masterKeyPair = AsymmetricCryptoUtil.generateKey();

        // Encrypt master private key with derived key
        IvParameterSpec iv = SymmetricCryptoUtil.generateIv();
        String encryptedKey = SymmetricCryptoUtil.encrypt(derivedKey, iv, Base64.getEncoder().encodeToString(masterKeyPair.getPrivate().getEncoded()));

        User user = new User()
            .setFirstName(registerRequestDto.getFirstName())
            .setLastName(registerRequestDto.getLastName())
            .setEmail(registerRequestDto.getEmail())
            .setPassword(passwordEncoder.encode(registerRequestDto.getPassword()))
            .setRegisteredAt(System.currentTimeMillis() / 1000)
            .setKeyEncryptionSalt(Base64.getEncoder().encodeToString(salt))
            .setMasterPrivateKeyIv(Base64.getEncoder().encodeToString(iv.getIV()))
            .setMasterPrivateKey(encryptedKey)
            .setMasterPublicKey(Base64.getEncoder().encodeToString(masterKeyPair.getPublic().getEncoded()))
            .setRole(roleRepository.findByName(Role.USER).get());

        userRepository.save(user);

        /*
        Authentication auth = authenticate(registerRequestDto.getEmail(), registerRequestDto.getPassword());
        createAuthSession(session, auth, registerRequestDto.getPassword());
         */
    }

    public void changePassword(HttpSession session, ChangePasswordRequestDto changePasswordRequestDto) throws IncorrectOldPasswordException {
        User user = getLoggedInUser();
        if (!passwordEncoder.matches(changePasswordRequestDto.getOldPassword(), user.getPassword())) {
            throw new IncorrectOldPasswordException();
        }

        // Generate key derived from new password
        byte[] salt = SymmetricCryptoUtil.generateSalt();
        SecretKey derivedKey = SymmetricCryptoUtil.deriveKey(salt, changePasswordRequestDto.getNewPassword());

        // Encrypt existing master private key with new derived key
        IvParameterSpec iv = SymmetricCryptoUtil.generateIv();
        String encryptedKey = SymmetricCryptoUtil.encrypt(derivedKey, iv, Base64.getEncoder().encodeToString(getMasterPrivateKey(session).getEncoded()));

        user.setPassword(passwordEncoder.encode(changePasswordRequestDto.getNewPassword()))
            .setKeyEncryptionSalt(Base64.getEncoder().encodeToString(salt))
            .setMasterPrivateKeyIv(Base64.getEncoder().encodeToString(iv.getIV()))
            .setMasterPrivateKey(encryptedKey);

        userRepository.save(user);
        createAuthSession(session, authenticate(user.getEmail(), changePasswordRequestDto.getNewPassword()), changePasswordRequestDto.getNewPassword());
    }

    private Authentication authenticate(String email, String password) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(email, password);
        return authManager.authenticate(token);
    }

    private void createAuthSession(HttpSession session, Authentication auth, String password) {
        SecurityContext context = SecurityContextHolder.getContext();
        context.setAuthentication(auth);
        session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, context);

        User user = (User) auth.getPrincipal();
        byte[] salt = Base64.getDecoder().decode(user.getKeyEncryptionSalt());
        SecretKey passwordKey = SymmetricCryptoUtil.deriveKey(salt, password);
        session.setAttribute("passwordKey", passwordKey);
    }
}
