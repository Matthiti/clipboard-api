package eu.roelink.clipboardapi.service;

import eu.roelink.clipboardapi.model.User;
import eu.roelink.clipboardapi.util.AsymmetricCryptoUtil;
import eu.roelink.clipboardapi.util.SymmetricCryptoUtil;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.servlet.http.HttpSession;
import java.security.PrivateKey;
import java.util.Base64;

public abstract class BaseService {

    public User getLoggedInUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            return null;
        }
        return (User) auth.getPrincipal();
    }

    public PrivateKey getMasterPrivateKey(HttpSession session) {
        User user = getLoggedInUser();
        SecretKey passwordKey = (SecretKey) session.getAttribute("passwordKey");
        IvParameterSpec iv = new IvParameterSpec(Base64.getDecoder().decode(user.getMasterPrivateKeyIv()));
        String dataKeyStr = SymmetricCryptoUtil.decrypt(passwordKey, iv, user.getMasterPrivateKey());
        return AsymmetricCryptoUtil.privateKeyFromBytes(Base64.getDecoder().decode(dataKeyStr));
    }
}
