package eu.roelink.clipboardapi.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class DecryptedClipboardItem {

    private Long id;

    private String content;

    private Long pastedAt;
}
