package eu.roelink.clipboardapi.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "clipboard_items")
public class ClipboardItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition = "TEXT")
    private String encryptedContent;

    private String encryptionIv;

    private Long pastedAt;

    @ManyToOne
    @JoinColumn(name = "clipboard_id", nullable = false)
    private Clipboard clipboard;
}
