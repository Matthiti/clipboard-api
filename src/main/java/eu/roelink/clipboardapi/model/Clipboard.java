package eu.roelink.clipboardapi.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "clipboards")
public class Clipboard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "clipboard", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private List<ClipboardUser> clipboardUsers;

    @OneToMany(mappedBy = "clipboard", cascade = CascadeType.ALL)
    private List<ClipboardItem> items;

    public boolean isOwner(User user) {
        return clipboardUsers
            .stream()
            .filter(u -> u.getUserId().equals(user.getId()))
            .findFirst()
            .get()
            .getIsOwner();
    }
}
