package eu.roelink.clipboardapi.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@IdClass(ClipboardUser.ClipboardUserId.class)
@Table(name = "clipboard_users")
public class ClipboardUser {

    @Id
    @Column(name = "clipboard_id")
    private Long clipboardId;

    @Id
    @Column(name = "user_id")
    private Long userId;

    private String encryptionKey;

    private Boolean isOwner;

    @ManyToOne
    @JoinColumn(name = "clipboard_id", nullable = false, insertable = false, updatable = false)
    private Clipboard clipboard;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false, insertable = false, updatable = false)
    private User user;

    @EqualsAndHashCode
    public static class ClipboardUserId implements Serializable {
        private Long clipboardId;
        private Long userId;
    }
}
