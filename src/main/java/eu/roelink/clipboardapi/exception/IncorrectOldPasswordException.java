package eu.roelink.clipboardapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Incorrect old password")
public class IncorrectOldPasswordException extends Exception {
}
