package eu.roelink.clipboardapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT, reason = "Email is already registered")
public class EmailAlreadyRegisteredException extends Exception {
}
