package eu.roelink.clipboardapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "You are not the owner of this clipboard")
public class NotOwnerOfClipboardException extends Exception {
}
