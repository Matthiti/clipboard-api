package eu.roelink.clipboardapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Model is not found")
public class ModelNotFoundException extends Exception {

    public ModelNotFoundException() {}

    public ModelNotFoundException(String model) {
        super(String.format("%s not found", model));
    }
}
