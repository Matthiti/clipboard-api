package eu.roelink.clipboardapi.controller;

import eu.roelink.clipboardapi.dto.request.ClipboardItemRequestDto;
import eu.roelink.clipboardapi.dto.response.DecryptedClipboardItemResponseDto;
import eu.roelink.clipboardapi.exception.ModelNotFoundException;
import eu.roelink.clipboardapi.service.ClipboardItemService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/clipboards/{clipboardId}/items")
@AllArgsConstructor
public class ClipboardItemController {

    private ClipboardItemService clipboardItemService;

    @GetMapping("")
    public List<DecryptedClipboardItemResponseDto> getAllClipboardItems(HttpSession session, @PathVariable Long clipboardId) throws ModelNotFoundException {
        return clipboardItemService.getAllDecryptedClipboardItems(session, clipboardId)
            .stream()
            .map(DecryptedClipboardItemResponseDto::from)
            .collect(Collectors.toList());
    }

    @GetMapping("{itemId}")
    public DecryptedClipboardItemResponseDto getClipboardItem(HttpSession session, @PathVariable Long clipboardId, @PathVariable Long itemId) throws ModelNotFoundException {
        return DecryptedClipboardItemResponseDto.from(
            clipboardItemService.getDecryptedClipboardItem(session, clipboardId, itemId)
        );
    }

    @PostMapping("")
    public DecryptedClipboardItemResponseDto createClipboardItem(HttpSession session, @PathVariable Long clipboardId, @Valid @RequestBody ClipboardItemRequestDto clipboardItemRequestDto) throws ModelNotFoundException {
        return DecryptedClipboardItemResponseDto.from(
            clipboardItemService.createClipboardItem(session, clipboardId, clipboardItemRequestDto)
        );
    }

    @PutMapping("{itemId}")
    public DecryptedClipboardItemResponseDto updateClipboardItem(HttpSession session, @PathVariable Long clipboardId, @PathVariable Long itemId, @Valid @RequestBody ClipboardItemRequestDto clipboardItemRequestDto) throws ModelNotFoundException {
        return DecryptedClipboardItemResponseDto.from(
            clipboardItemService.updateClipboardItem(session, clipboardId, itemId, clipboardItemRequestDto)
        );
    }

    @DeleteMapping("{itemId}")
    public void deleteClipboardItem(@PathVariable Long clipboardId, @PathVariable Long itemId) throws ModelNotFoundException {
        clipboardItemService.deleteClipboardItem(clipboardId, itemId);
    }
}
