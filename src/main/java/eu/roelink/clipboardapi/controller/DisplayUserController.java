package eu.roelink.clipboardapi.controller;

import eu.roelink.clipboardapi.dto.response.DisplayUserResponseDto;
import eu.roelink.clipboardapi.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/display-users")
@AllArgsConstructor
public class DisplayUserController {

    private UserService userService;

    @GetMapping("")
    public List<DisplayUserResponseDto> getAllDisplayUsers() {
        return userService.getAllUsers()
            .stream()
            .map(DisplayUserResponseDto::from)
            .collect(Collectors.toList());
    }
}
