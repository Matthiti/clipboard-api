package eu.roelink.clipboardapi.controller;

import eu.roelink.clipboardapi.dto.request.LoginRequestDto;
import eu.roelink.clipboardapi.dto.request.ChangePasswordRequestDto;
import eu.roelink.clipboardapi.dto.response.UserResponseDto;
import eu.roelink.clipboardapi.exception.IncorrectOldPasswordException;
import eu.roelink.clipboardapi.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
@AllArgsConstructor
public class AuthController {

    private final AuthService authService;

    @GetMapping("me")
    public UserResponseDto me() {
        return UserResponseDto.from(authService.getLoggedInUser());
    }

    @PostMapping("login")
    public void login(HttpSession session, @Valid @RequestBody LoginRequestDto loginRequestDto) {
        authService.login(session, loginRequestDto);
    }

    @PostMapping("logout")
    public void logout(HttpSession session) {
        authService.logout(session);
    }

    @PostMapping("change-password")
    public void changePassword(HttpSession session, @Valid @RequestBody ChangePasswordRequestDto changePasswordRequestDto) throws IncorrectOldPasswordException {
        authService.changePassword(session, changePasswordRequestDto);
    }

    // Disable registration via API for now
    /*
    @PostMapping("register")
    public void register(HttpSession session, @Valid @RequestBody RegisterRequestDto registerRequestDto) throws EmailAlreadyRegisteredException {
        authService.register(session, registerRequestDto);
    }
     */
}
