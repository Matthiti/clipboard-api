package eu.roelink.clipboardapi.controller;

import eu.roelink.clipboardapi.dto.request.ClipboardRequestDto;
import eu.roelink.clipboardapi.dto.request.ShareClipboardRequestDto;
import eu.roelink.clipboardapi.dto.response.ClipboardResponseDto;
import eu.roelink.clipboardapi.exception.ModelNotFoundException;
import eu.roelink.clipboardapi.exception.NotOwnerOfClipboardException;
import eu.roelink.clipboardapi.service.ClipboardService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/clipboards")
@AllArgsConstructor
public class ClipboardController {

    private final ClipboardService clipboardService;

    @GetMapping("")
    public List<ClipboardResponseDto> getAllClipboards() {
        return clipboardService.getAllClipboardsOfLoggedInUser()
            .stream()
            .map(ClipboardResponseDto::from)
            .collect(Collectors.toList());
    }

    @GetMapping("{id}")
    public ClipboardResponseDto getClipboard(@PathVariable Long id) throws ModelNotFoundException {
        return ClipboardResponseDto.from(
            clipboardService.getClipboard(id)
        );
    }

    @PostMapping("")
    public ClipboardResponseDto createClipboard(@Valid @RequestBody ClipboardRequestDto clipboardRequestDto) {
        return ClipboardResponseDto.from(
            clipboardService.createClipboard(clipboardRequestDto)
        );
    }

    @PutMapping("{id}")
    public ClipboardResponseDto updateClipboard(@PathVariable Long id, @Valid @RequestBody ClipboardRequestDto clipboardRequestDto) throws ModelNotFoundException, NotOwnerOfClipboardException {
        return ClipboardResponseDto.from(
            clipboardService.updateClipboard(id, clipboardRequestDto)
        );
    }

    @PostMapping("{id}/share")
    public void shareClipboard(HttpSession session, @PathVariable Long id, @Valid @RequestBody ShareClipboardRequestDto shareClipboardRequestDto) throws ModelNotFoundException, NotOwnerOfClipboardException {
        clipboardService.shareClipboard(session, id, shareClipboardRequestDto);
    }

    @DeleteMapping("{id}")
    public void deleteClipboard(@PathVariable Long id) throws ModelNotFoundException, NotOwnerOfClipboardException {
        clipboardService.deleteClipboard(id);
    }
}
