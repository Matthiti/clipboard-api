package eu.roelink.clipboardapi.controller;

import eu.roelink.clipboardapi.dto.request.UserRequestDto;
import eu.roelink.clipboardapi.dto.response.UserResponseDto;
import eu.roelink.clipboardapi.exception.EmailAlreadyRegisteredException;
import eu.roelink.clipboardapi.exception.ModelNotFoundException;
import eu.roelink.clipboardapi.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("")
    @PreAuthorize("hasRole('ADMIN')")
    public List<UserResponseDto> getAllUsers() {
        return userService.getAllUsers()
            .stream()
            .map(UserResponseDto::from)
            .collect(Collectors.toList());
    }

    @GetMapping("{id}")
    @PreAuthorize("#id == principal.id or hasRole('ADMIN')")
    public UserResponseDto getUser(@PathVariable Long id) throws ModelNotFoundException {
        return UserResponseDto.from(
            userService.getUser(id)
        );
    }

    @PutMapping("{id}")
    @PreAuthorize("#id == principal.id or hasRole('ADMIN')")
    public UserResponseDto updateUser(@PathVariable Long id, @Valid @RequestBody UserRequestDto userRequestDto) throws ModelNotFoundException, EmailAlreadyRegisteredException {
        return UserResponseDto.from(
            userService.updateUser(id, userRequestDto)
        );
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteUser(@PathVariable Long id) throws ModelNotFoundException {
        userService.deleteUser(id);
    }
}
